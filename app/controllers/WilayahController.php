<?php

/**
* Ini class untuk menampilkan halaman wilayah
*/

class WilayahController extends BaseController
{
	public function showWilayah()
	{
		return View::make('wilayah');
	}
}
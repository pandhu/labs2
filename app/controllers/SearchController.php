
<?php


class SearchController extends BaseController {


	public function getSearchData($qSearch)
	{
		if (Request::isMethod('get')){
			$wilayah = Wilayah::where('nama_wilayah', 'like', '{$qSearch}')->get();
			$subwilayah = Subwilayah::where('nama_subwilayah', 'like', '{$qSearch}')->get();
			$wisata = Tempat_Wisata::where('nama_wisata', 'like', '{$qSearch}')->get();
			
			$data = array(
			    'wilayah'  => $wilayah,
			    'subwilayah' => $subwilayah,
			    'wisata' => $wisata
			);
			return View::make('search', $data);	
		}
		
	}

}
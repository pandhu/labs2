<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Penginapan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('penginapan', function($table)
		{
    		$table->increments('id');
    		$table->string('nama_penginapan');
     		$table->string('nama_pemilik');
    		$table->string('deskripsi');
    		$table->string('jenis');
    		$table->string('koordinat');
    		$table->integer('harga');
    		$table->integer('id_subwilayah')->unsigned();
    		$table->foreign('id_subwilayah')->references('id')->on('subwilayah');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('penginapan');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Produk extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produk', function($table)
		{
    		$table->integer('id_tempat')->unsigned();
    		$table->foreign('id_tempat')->references('id')->on('tempat_oleh_oleh');
    		$table->increments('id_produk');
    		$table->string('nama_produk');
    		$table->integer('harga');
    		$table->string('deskripsi');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produk');
	}

}

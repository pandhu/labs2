<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subwilayah extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subwilayah', function($table)
		{
    		$table->increments('id');
    		$table->string('nama_subwilayah');
    		$table->integer('id_wilayah')->unsigned();
    		$table->foreign('id_wilayah')->references('id')->on('wilayah');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subwilayah');
	}

}

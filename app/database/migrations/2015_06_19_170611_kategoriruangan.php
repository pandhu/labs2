<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kategoriruangan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kategori_ruangan', function($table)
		{
    		$table->integer('id_penginapan')->unsigned();
    		$table->foreign('id_penginapan')->references('id')->on('penginapan');
    		$table->increments('id_ruangan');
    		$table->string('nama_ruangan');
     		$table->integer('harga');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kategori_ruangan');
	}

}

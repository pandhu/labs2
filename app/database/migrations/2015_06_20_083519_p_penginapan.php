<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PPenginapan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('p_penginapan', function($table)
		{
    		$table->integer('id_planner')->unsigned();
    		$table->foreign('id_planner')->references('id_planner')->on('planner');
    		$table->integer('id_penginapan')->unsigned();
    		$table->foreign('id_penginapan')->references('id')->on('penginapan');
    		$table->primary(array('id_planner', 'id_penginapan'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('p_penginapan');
	}

}

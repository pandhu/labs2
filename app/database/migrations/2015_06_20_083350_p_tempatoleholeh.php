<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTempatoleholeh extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('p_tempatoleholeh', function($table)
		{
    		$table->integer('id_planner')->unsigned();
    		$table->foreign('id_planner')->references('id_planner')->on('planner');
    		$table->integer('id_tempatoleholeh')->unsigned();
    		$table->foreign('id_tempatoleholeh')->references('id')->on('tempat_oleh_oleh');
    		$table->primary(array('id_planner', 'id_tempatoleholeh'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('p_tempatoleholeh');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTempatwisata extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('p_tempatwisata', function($table)
		{
    		$table->integer('id_planner')->unsigned();
    		$table->foreign('id_planner')->references('id_planner')->on('planner');
    		$table->integer('id_tempatwisata')->unsigned();
    		$table->foreign('id_tempatwisata')->references('id')->on('tempat_wisata');
    		$table->primary(array('id_planner', 'id_tempatwisata'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('p_tempatwisata');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gallery extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gallery', function($table)
		{
    		$table->increments('id_foto');
    		$table->integer('id_tempatwisata')->unsigned();
    		$table->foreign('id_tempatwisata')->references('id')->on('tempat_wisata');
    		$table->string('foto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gallery');
	}

}

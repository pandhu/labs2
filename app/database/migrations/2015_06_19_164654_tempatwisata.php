<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tempatwisata extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tempat_wisata', function($table)
		{
    		$table->increments('id');
    		$table->string('nama_wisata');
    		$table->string('alamat');
    		$table->string('jenis');
    		$table->string('deskripsi');
    		$table->string('keunikan')->nullable();
    		$table->string('wahana')->nullable();
    		$table->string('koordinat');
    		$table->integer('harga_tiket_masuk');
    		$table->integer('id_subwilayah')->unsigned();
    		$table->foreign('id_subwilayah')->references('id')->on('subwilayah');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tempat_wisata');
	}

}

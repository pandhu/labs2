<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tempatoleholeh extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tempat_oleh_oleh', function($table)
		{
    		$table->increments('id');
    		$table->string('nama_tempat');
    		$table->string('alamat');
    		$table->string('jenis');
    		$table->string('koordinat');
    		$table->integer('id_subwilayah')->unsigned();
    		$table->foreign('id_subwilayah')->references('id')->on('subwilayah');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tempat_oleh_oleh');
	}

}

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Jalan Yuk!</title>

	<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('css/main.css')}}" rel="stylesheet">
	<link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">
	<script>
		function showSuggestion(str) {
		    if (str == "") {
		        document.getElementById("suggestion").innerHTML = "";
		        return;
		    } else { 
		        if (window.XMLHttpRequest) {
		            // code for IE7+, Firefox, Chrome, Opera, Safari
		            xmlhttp = new XMLHttpRequest();
		        } else {
		            // code for IE6, IE5
		            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		        }
		        xmlhttp.onreadystatechange = function() {
		            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		                document.getElementById("suggestion").innerHTML = xmlhttp.responseText;
		            }
		        }
		        xmlhttp.open("GET","./search/"+str,true);
		        xmlhttp.send();
		    }
		}
	</script>
</head>
<body>
	<!-- <div class="page-wrap"> -->
		<header class="header--home">
			<div class="header__logo">
				<a href="#"><img src="{{asset('/images/logo2.png')}}" width="250px" height="120px"></a>
			</div>
		</header>
		<br>
		<br>
		<br>
		<br>
		<br>
		<section class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1 class="content__site-quote text-center">"Rencanakan Wisatamu!"</h1>
						
						<br>
						<br>
						<br>
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<h4 class="content__site-label">Masukkan wilayah atau nama tempat wisata</h6>
						
							<div class="content__search-input">
								<div class="input-group">
									<input type="text" class="form-control input-lg" placeholder="Co: Sleman atau Borobudur">
									<span class="input-group-btn">
										<button type="button" class="btn-info btn-lg">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
									<div id="suggestion">sugession</div>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
				</div>
			</div>
		</section>
	<!-- </div> -->

	<footer class="site-footer">
		<p>Copyright &copy; Tafakur Alam 2015</p>
	</footer>
	
	<script src="{{asset('/js/jquery-1.11.3.js')}}"></script>
	<script src="{{asset('/js/bootstrap.js')}}"></script>
</body>
</html>
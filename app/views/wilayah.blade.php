<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Jalan Yuk!</title>

	<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('css/main.css')}}" rel="stylesheet">
	<link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">
</head>
<body>
	<header class="header--wilayah">
		<!-- <div class="header__search">
			<div class="input-group">
				<input type="text" class="form-control input-lg">
				<span class="input-group-btn">
					<button type="button" class="btn-info btn-lg">
						<i class="glyphicon glyphicon-search"></i>
					</button>
				</span>
			</div>
		</div> -->
		<div class="header__logo">
			<a href="#"><img src="{{asset('/images/logo2.png')}}" width="250px" height="120px"></a>
		</div>
	</header>

	<section class="map-content">
		<div class="tempat-wisata">
			<div class="judul-tempat">
				Tempat Wisata
			</div>
			<div class="isi-tempat">
				<ul>
					<li><a href="#">Parangtritis</a></li>
					<li><a href="#">Malioboro</a></li>
					<li><a href="#">Keraton Yogyakarta</a></li>
				</ul>
			</div>
			<div class="judul-penginapan">
				Penginapan
			</div>
			<div class="isi-tempat">
				<ul>
					<li><a href="#">Hotel Aston</a></li>
					<li><a href="#">Hotel Mulia</a></li>
					<li><a href="#">Hotel Hyatt</a></li>
				</ul>
			</div>
		</div>
		<div class="map-content">
			
		</div>
		<div class="rencana-wisata">
			<div class="judul-rencana">
				Rencana Wisata
			</div>
			<div class="isi-rencana">
				<ul>
					<li><a href="#">Rencana A</a></li>
					<li><a href="#">Rencana B</a></li>
					<li><a href="#">Rencana C</a></li>
				</ul>
			</div>
		</div>
	</section>

	<footer class="site-footer">
		<p>Copyright &copy; Tafakur Alam 2015</p>
	</footer>
	
	<script src="../public/js/jquery-1.11.3.js"></script>
	<script src="../public/js/bootstrap.js"></script>
</body>
</html>